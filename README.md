# halo-icons-git

Halo is an icon theme for Linux desktops, the set is inspired by the latest flat design trend from Alejandro Camarena

How to clone this repo:

```
git clone https://gitlab.com/reborn-os-team/themes-and-icons/halo-icons-git.git
```

